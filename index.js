//              i  0   1  2  3   4     5    length: 5
const valueList = [2, 47, 5, 41, 4];// x

//  init          condition de     incrementation
//  de l'index     poursuite       de l'index
/*
const newValues = [];
for (let i = 0; i < valueList.length; i++) {
  const currentValue = valueList[i];
  newValues.push(currentValue * 2);
}
console.log(newValues);
*/

// for each : pour chaque élément currentValue de mon tableau valueList
// for (const personne of bus)
/*
const newValues = [];
for (const eachValue of valueList) {
  newValues.push(eachValue * 2);
}
console.log(newValues);
*/

// map : appliquer une modification sur chaque élément du tableau, et obtenir un nouveau tableau
// const nouveauTableau = tableau.map(function (element) { return transformation })
// const nouveauTableau = tableau.map((element) => transformation)
// const newValues = valueList.map((value) => value * 2);
/*
const newValues = valueList.map((value) => {
  const updatedValue = value * 2;
  return updatedValue;
});
console.log(newValues);
*/

function doubleValue(value) {
  return value * 2;
}
console.log(doubleValue(42));

// permet d'obtenir un nouveau tableau avec toutes les valeurs doublées
function doubleAllValues(valueArray) {
  const newValueArray = [];
  for (const eachValue of valueArray) {
    const newValue = doubleValue(eachValue);
    newValueArray.push(newValue);
  }
  return newValueArray;
}
console.log(doubleAllValues(valueList))

function tripleValue(value) {
  return value * 3;
}

// abstraction
function myMap(valueArray, action) {
  const newValueArray = [];
  for (const eachValue of valueArray) {
    const newValue = action(eachValue);
    newValueArray.push(newValue);
  }
  return newValueArray;
}

valueList.map((value) => value * 3)
const result = myMap(valueList, (value) => value * 3)
//const result = myMap(valueList, tripleValue)
console.log(result)

// callback : quand on passe une fonction en paramètre d'une fonction
document.addEventListener('click', function(event) {

})

// louis est un callback
function inviterDesAmis(listeDesInvites, louis) {
  const quiPeutVenir = louis(listeDesInvites)
}